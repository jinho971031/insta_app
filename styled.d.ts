import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    dark: boolean;

    fontColor: string;
    bgColor: string;
    boxColor: string;
    borderColor: string;
    seprator: string;
    link: string;
    hoverColor: string;
    modalbgColor: string;
    modalboxColor: string;
    modalbdColor: string;

    black: string;
    white: string;
    blue: string;
    darkBlue: string;

    liteGray: string;
    gray: string;
    darkGray: string;
  }
}
