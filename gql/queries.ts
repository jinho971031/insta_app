import { gql } from "@apollo/client";
import { PHOTO_FRAGMENT } from "./fragments";

export const FEED_QUERY = gql`
  query Feed($lastID: Int) {
    Feed(lastID: $lastID) {
      ...PhotoFragment
      likes {
        id
        user {
          username
          avatar
        }
      }
    }
  }
  ${PHOTO_FRAGMENT}
`;

export const ME_QUERY = gql`
  query me {
    me {
      username
      avatar
    }
  }
`;
