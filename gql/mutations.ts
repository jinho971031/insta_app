import { gql } from "@apollo/client";

/* 
  signup : 이 프로젝트 안에서 사용할 이름
  createAccount : 실제 사용할 graphql 쿼리 이름
*/
export const CREATE_ACCOUNT_MUTATION = gql`
  mutation signup(
    $name: String!
    $email: String!
    $username: String!
    $password: String!
  ) {
    createAccount(
      name: $name
      email: $email
      username: $username
      password: $password
    ) {
      ok
      error
    }
  }
`;

export const LOGIN_MUTATION = gql`
  mutation login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      ok
      token
      error
    }
  }
`;

export const PHOTO_LIKE_MUTATION = gql`
  mutation photolike($photoId: Int!) {
    togglePhotoLike(photoId: $photoId) {
      ok
    }
  }
`;
