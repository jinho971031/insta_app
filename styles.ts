import { DefaultTheme } from "styled-components/native";

export const lightTheme: DefaultTheme = {
  dark: false,
  fontColor: "rgb(38,38,38)",
  bgColor: "#fff",
  boxColor: "#FAFAFA",
  borderColor: "#e0e0e0",
  seprator: "rgb(219,219,219)",
  link: "rgb(142,142,142)",
  hoverColor: "rgb(219,219,219)",
  modalbgColor: "#FFFFFF",
  modalboxColor: "#EFEFEF",
  modalbdColor: "#e0e0e0",

  black: "rgb(38,38,38)",
  white: "#fff",
  blue: "#0094F6",
  darkBlue: "#385185",
  liteGray: "rgb(219, 219, 219)",
  gray: "#565656",
  darkGray: "#262626",
};

export const darkTheme: DefaultTheme = {
  dark: true,
  fontColor: "#fff",
  bgColor: "#000",
  boxColor: "#121212",
  borderColor: "#1E222D",
  seprator: "#8e8e8e",
  link: "#fff",
  hoverColor: "#262B3E",
  modalbgColor: "#262626",
  modalboxColor: "#565656",
  modalbdColor: "#565656",

  black: "rgb(38,38,38)",
  white: "#fff",
  blue: "#0094F6",
  darkBlue: "#385185",
  liteGray: "rgb(219, 219, 219)",
  gray: "#565656",
  darkGray: "#262626",
};
