import React from "react";
import { useState } from "react";
import { Appearance } from "react-native";
import { AppearanceProvider } from "react-native-appearance";
import { NavigationContainer } from "@react-navigation/native";

import { ThemeProvider } from "styled-components/native";

import * as Font from "expo-font";
import { Asset } from "expo-asset";
import AppLoading from "expo-app-loading";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";

import { darkTheme, lightTheme } from "./styles";
import LoggedOutNav from "./navigators/LoggedOutNav";
import LoggedInNav from "./navigators/LoggedInNav";

import { ApolloProvider, useReactiveVar } from "@apollo/client";
import { defaultClient, isLoggedInVar, tokenVar } from "./apollo";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { StatusBar } from "expo-status-bar";

export default function App() {
  // 로딩 상태 체크할 state만들기
  const [loading, setLoading] = useState(true);

  // 로딩 끝나면, 실행될 함수 만들기
  const onFinish = () => setLoading(false);

  const [darkMode, setDarkMode] = useState(Appearance.getColorScheme());

  Appearance.addChangeListener(({ colorScheme }) => {
    setDarkMode(colorScheme);
  });

  const isLoggedIn = useReactiveVar(isLoggedInVar);

  //--------------------------------------------------------------------
  const preloadAssets = async () => {
    const fontsToLoad = [Ionicons.font, MaterialCommunityIcons.font];
    const fontPromises = fontsToLoad.map((font: any) => Font.loadAsync(font));
    const imagesToLoad = [
      require("./assets/logo.png"),
      require("./assets/logo_white.png"),
    ];
    const imagePromises = imagesToLoad.map((img: any) => Asset.loadAsync(img));
    await Promise.all<Promise<void> | Promise<Asset[]>>([
      ...fontPromises,
      ...imagePromises,
    ]);
  };
  // 로딩 중에 미리 처리할 작업들
  const preload = async () => {
    const token = await AsyncStorage.getItem("token");
    console.log(token);
    if (token) {
      // ReactiveVar를 변경하면, rendering이 일어난다! 렌더링 두번?
      tokenVar(token);
      isLoggedInVar(true);
    }

    // startAsync에는 반드시 Promise가 주어져야한다.
    return preloadAssets();
  };

  if (loading) {
    // 앱 로딩 불러와 주기
    return (
      <AppLoading
        startAsync={preload}
        onError={console.warn}
        onFinish={onFinish}
      />
    );
  }

  return (
    <ApolloProvider client={defaultClient}>
      <AppearanceProvider>
        <ThemeProvider theme={darkMode === "dark" ? darkTheme : lightTheme}>
          <NavigationContainer>
            {darkMode === "dark" ? (
              <StatusBar style="light" />
            ) : (
              <StatusBar style="dark" />
            )}
            {isLoggedIn ? <LoggedInNav /> : <LoggedOutNav />}
          </NavigationContainer>
        </ThemeProvider>
      </AppearanceProvider>
    </ApolloProvider>
  );
}
