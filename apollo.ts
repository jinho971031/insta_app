import { setContext } from "@apollo/client/link/context";
import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  makeVar,
  useReactiveVar,
} from "@apollo/client";
import AsyncStorage from "@react-native-async-storage/async-storage";

//-------------------------------------------------------------

export const isLoggedInVar = makeVar(false);
export const tokenVar = makeVar("");

const TOKEN = "token";

export const logUserIn = async (token: string) => {
  await AsyncStorage.setItem(TOKEN, token);
  isLoggedInVar(true);
  tokenVar(token);
};

/* 
await AsyncStorage.multiSet([
    [TOKEN, token],
    ["loggedIn", "yes"],
  ]);
*/

export const logUserOut = async () => {
  await AsyncStorage.removeItem(TOKEN);
  isLoggedInVar(false);
  tokenVar("");
};

//------------------------------------------------------------

const NGROK = "https://fc77-183-96-20-168.ngrok.io/";

const httpLink = createHttpLink({
  uri: `${NGROK}graphql`,
});

/* setContext(request, previousContext) */
const authLink = setContext((_, { headers }: any) => {
  return {
    headers: {
      ...headers,
      authorization: tokenVar(),
    },
  };
});

export const defaultClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

// Modal Open ---------------------------------------------------

export const modalVar = makeVar(false);
