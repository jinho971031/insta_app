import React, { useContext } from "react";
import { ThemeContext } from "styled-components/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";

import SharedStackNav from "./SharedStackNav";
import { useQuery } from "@apollo/client";
import { FEED_QUERY, ME_QUERY } from "../gql/queries";
import { ActivityIndicator } from "react-native";
import ScreenLayout from "../components/ScreenLayout";
import MyProfile from "../screens/MyProfile";

const Tabs = createBottomTabNavigator();

const focusIconSize = 30;

export default function LoggedInNav() {
  const { bgColor, fontColor, borderColor } = useContext(ThemeContext);

  // login preset Query
  const { loading: meLoading } = useQuery(ME_QUERY);
  const { loading: feedLoading } = useQuery(FEED_QUERY);

  const loading = meLoading || feedLoading;

  return loading ? (
    <ScreenLayout>
      <ActivityIndicator color={fontColor} />
    </ScreenLayout>
  ) : (
    <Tabs.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: bgColor,
          borderTopColor: borderColor,
          height: 70,
        },
        tabBarActiveTintColor: fontColor,
        tabBarShowLabel: false,
      }}
    >
      <Tabs.Screen
        name="TabsFeed"
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <MaterialCommunityIcons
              name="home-variant"
              color={color}
              size={focused ? focusIconSize : size}
              focused={focused}
            />
          ),
        }}
      >
        {() => <SharedStackNav screenName="Feed" />}
      </Tabs.Screen>
      <Tabs.Screen
        name="TabsSearch"
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Ionicons
              name="search"
              color={color}
              size={focused ? focusIconSize : size}
              focused={focused}
            />
          ),
        }}
      >
        {() => <SharedStackNav screenName="Video" />}
      </Tabs.Screen>
      <Tabs.Screen
        name="TabsVideo"
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Ionicons
              name="videocam"
              color={color}
              size={focused ? focusIconSize : size}
              focused={focused}
            />
          ),
        }}
      >
        {() => <SharedStackNav screenName="Video" />}
      </Tabs.Screen>
      <Tabs.Screen
        name="TabsMarket"
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <MaterialCommunityIcons
              name="shopping"
              color={color}
              size={focused ? focusIconSize : size}
              focused={focused}
            />
          ),
        }}
      >
        {() => <SharedStackNav screenName="Market" />}
      </Tabs.Screen>
      <Tabs.Screen
        name="MyProfile"
        component={MyProfile}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Ionicons
              name="person"
              color={color}
              size={focused ? focusIconSize : size}
              focused={focused}
            />
          ),
        }}
      />
    </Tabs.Navigator>
  );
}
