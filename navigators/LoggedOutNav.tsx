import React, { useContext } from "react";
import { ThemeContext } from "styled-components/native";
import { createStackNavigator } from "@react-navigation/stack";

import CreateAccount from "../screens/auth/CreateAccount";
import Welcome from "../screens/auth/Welcome";
import Login from "../screens/auth/Login";

const Stack = createStackNavigator();
export default function LoggedOutNav() {
  const { fontColor } = useContext(ThemeContext);

  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitle: "",
        headerTransparent: true,
        headerTintColor: fontColor,
      }}
    >
      <Stack.Screen
        name="Welcome"
        component={Welcome}
        options={{ headerShown: false }}
      />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="CreateAccount" component={CreateAccount} />
    </Stack.Navigator>
  );
}
