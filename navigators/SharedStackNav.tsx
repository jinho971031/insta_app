import React, { useContext } from "react";
import { ThemeContext } from "styled-components/native";
import { createStackNavigator } from "@react-navigation/stack";

import Feed from "../screens/Feed";
import Market from "../screens/Market";
import Profile from "../screens/Profile";
import Search from "../screens/Search";
import Video from "../screens/Video";
import Photo from "../screens/Photo";
import Notifications from "../screens/Notifications";
import Likes from "../screens/Likes";

const Stack = createStackNavigator();

export default function SharedStackNav({ screenName }: any) {
  const { fontColor, bgColor } = useContext(ThemeContext);
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerTitle: "",
        headerTransparent: true,
        headerTintColor: fontColor,
      }}
    >
      {screenName === "Feed" ? (
        <Stack.Screen name="Feed" component={Feed} />
      ) : null}
      {screenName === "Search" ? (
        <Stack.Screen name="Search" component={Search} />
      ) : null}
      {screenName === "Video" ? (
        <Stack.Screen name="Video" component={Video} />
      ) : null}
      {screenName === "Market" ? (
        <Stack.Screen name="Market" component={Market} />
      ) : null}
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Photo" component={Photo} />
      <Stack.Screen name="Notifications" component={Notifications} />
      <Stack.Screen name="Likes" component={Likes} />
    </Stack.Navigator>
  );
}
