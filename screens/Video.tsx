import React from "react";
import styled from "styled-components/native";
import { Text, TouchableOpacity, View } from "react-native";
import { logUserOut } from "../apollo";

import { SharedLink } from "../components/Shared";
import AuthButton from "../components/auth/AuthButton";
import ScreenLayout from "../components/ScreenLayout";

const ButtonContainer = styled.View`
  margin-top: 20px;
`;

export default function Video() {
  const onSubmit = () => {
    console.log("Log Out");
    logUserOut();
  };
  return (
    <ScreenLayout>
      <Text>Hello</Text>
      <ButtonContainer>
        <AuthButton text="로그아웃" onPress={onSubmit} />
      </ButtonContainer>
    </ScreenLayout>
  );
}
