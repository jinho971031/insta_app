import React from "react";
import { TouchableOpacity } from "react-native";
import styled from "styled-components/native";

import AuthButton from "../../components/auth/AuthButton";
import AuthLayout from "../../components/auth/AuthLayout";

const SignUpLink = styled.Text`
  color: ${(props) => props.theme.blue};
  font-weight: 600;
  margin-top: 20px;
`;

export default function Welcome({ navigation }: any) {
  const goToLogin = () => navigation.navigate("Login");
  const goToCreateAccount = () => navigation.navigate("CreateAccount");

  return (
    <AuthLayout>
      <AuthButton
        text="새 계정 만들기"
        disabled={false}
        onPress={goToCreateAccount}
      />

      <TouchableOpacity onPress={goToLogin}>
        <SignUpLink>로그인</SignUpLink>
      </TouchableOpacity>
    </AuthLayout>
  );
}
