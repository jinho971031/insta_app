import React, { useRef } from "react";
import { TouchableOpacity, Alert } from "react-native";
import { Controller, useForm } from "react-hook-form";
import { logUserIn } from "../../apollo";

import { useMutation } from "@apollo/client";
import { LOGIN_MUTATION } from "../../gql/mutations";

import styled from "styled-components/native";
import AuthButton from "../../components/auth/AuthButton";
import AuthLayout from "../../components/auth/AuthLayout";
import AuthSeperator from "../../components/auth/AuthSeperator";
import { AuthInput } from "../../components/auth/AuthShared";
import FBLoginButton from "../../components/auth/FBLoginButton";
import { SharedText, SharedLink } from "../../components/Shared";

const FPLink = styled(SharedLink)`
  width: 100%;
  margin-bottom: 30px;
  text-align: right;
`;

const BottomView = styled.View`
  width: 120%;
  position: absolute;
  bottom: 0;

  flex-direction: row;
  justify-content: center;
  align-items: center;
  border: 0.5px solid ${(props) => props.theme.borderColor};
  padding: 15px;
`;

const SText = styled(SharedText)`
  margin-right: 3px;
`;

export default function Login({ navigation, route: { params } }: any) {
  // useRef
  const usernameRef = useRef(null);
  const passwordRef = useRef(null);
  const onNext = (nextOne: any) => {
    nextOne?.current?.focus();
  };
  const goToCreateAccount = () => navigation.navigate("CreateAccount");

  // useForm
  const { control, handleSubmit, formState, getValues } =
    useForm<LoginFormData>({
      mode: "onChange",
      defaultValues: {
        username: params?.username,
        password: params?.password,
      },
    });
  const { errors, isValid } = formState;

  // useMutation

  const onCompleted = async (data: any) => {
    const {
      login: { ok, error, token },
    } = data;

    if (ok) {
      console.log("Login");
      await logUserIn(token);
      return;
    }

    Alert.alert(
      "jinho2589님의 비밀번호가 잘못되었습니다",
      "입력된 비밀번호가 올바르지 않습니다. 다시 시도하세요.",
      [
        {
          text: "다시 시도",
          onPress: () => onNext(usernameRef),
          style: "cancel",
        },
      ]
    );
    return;
  };

  const [login, { loading }] = useMutation(LOGIN_MUTATION, { onCompleted });

  // onSubmit
  const onSubmit = () => {
    if (loading) {
      return;
    }
    const { username, password } = getValues();
    login({
      variables: {
        username,
        password,
      },
    });
  };

  //return
  return (
    <AuthLayout>
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <AuthInput
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            placeholder="전화번호, 사용자 이름 또는 이메일"
            returnKeyType="next"
            onSubmitEditing={() => onNext(passwordRef)}
            ref={usernameRef}
          />
        )}
        name="username"
        rules={{ required: true }}
      />
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <AuthInput
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            placeholder="비밀번호"
            secureTextEntry
            returnKeyType="done"
            onSubmitEditing={handleSubmit(onSubmit)}
            ref={passwordRef}
            lastOne={true}
          />
        )}
        name="password"
        rules={{ required: true }}
      />

      <FPLink>비밀번호를 잊으셨나요?</FPLink>
      <AuthButton
        text="로그인"
        disabled={!isValid}
        onPress={handleSubmit(onSubmit)}
        loading={loading}
      />
      <AuthSeperator />
      <FBLoginButton />
      <BottomView>
        <SText>계정이 없으신가요?</SText>
        <TouchableOpacity onPress={goToCreateAccount}>
          <SharedLink>가입하기</SharedLink>
        </TouchableOpacity>
      </BottomView>
    </AuthLayout>
  );
}

// onChangeText : 방금 바뀐 text를 인자로 돌려 주는 function
