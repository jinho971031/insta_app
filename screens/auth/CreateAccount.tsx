import React, { useRef } from "react";
import { useMutation } from "@apollo/client";
import { Controller, useForm } from "react-hook-form";

import AuthButton from "../../components/auth/AuthButton";
import AuthLayout from "../../components/auth/AuthLayout";
import { AuthInput } from "../../components/auth/AuthShared";

import FormError from "../../components/auth/FormError";

import { CREATE_ACCOUNT_MUTATION } from "../../gql/mutations";

export default function CreateAccount({ navigation }: any) {
  // useRef
  const nameRef = useRef(null);
  const usernameRef = useRef(null);
  const passwordRef = useRef(null);

  const onNext = (nextOne: any) => {
    nextOne?.current?.focus();
  };

  // useForm
  const { control, handleSubmit, formState, getValues } =
    useForm<SignUpFormData>({
      mode: "onChange",
    });
  const { errors, isValid } = formState;

  // useMutation
  const onCompleted = (data: any) => {
    const {
      createAccount: { error, ok },
    } = data;

    const { username, password } = getValues();

    if (ok) {
      navigation.navigate("Login", { username, password });
    }

    if (error) {
      alert(error);
    }
  };

  const [signup, { loading }] = useMutation(CREATE_ACCOUNT_MUTATION, {
    onCompleted,
  });

  // onSubmit
  const onSubmit = () => {
    const { email, name, username, password } = getValues();
    signup({
      variables: {
        email,
        name,
        username,
        password,
      },
    });
  };

  //return
  return (
    <AuthLayout>
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <AuthInput
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            placeholder="전화번호 or 이메일"
            keyboardType="email-address"
            returnKeyType="next"
            onSubmitEditing={() => onNext(nameRef)}
          />
        )}
        name="email"
        rules={{
          required: true,
          maxLength: {
            value: 100,
            message: "100자 이내로 작성해주세요.",
          },
          pattern: {
            value: /^[0-9a-zA-Z._%+-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,6}$/i,
            message: "이메일을 형식에 맞게 작성해주세요.",
          },
        }}
      />
      {errors?.email?.message ? (
        <FormError message={errors.email.message} />
      ) : null}

      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <AuthInput
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            placeholder="이름"
            returnKeyType="next"
            ref={nameRef}
            onSubmitEditing={() => onNext(usernameRef)}
          />
        )}
        name="name"
        rules={{
          required: true,
          pattern: {
            value: /^[A-Za-z가-힣]+$/i,
            message: "문자만 입력해주세요.",
          },
        }}
      />
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <AuthInput
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            placeholder="사용자 이름"
            returnKeyType="next"
            ref={usernameRef}
            onSubmitEditing={() => onNext(passwordRef)}
          />
        )}
        name="username"
        rules={{
          required: true,
          minLength: 5,
          pattern: {
            value: /^[A-Za-z0-9]+$/i,
            message: "영문, 숫자만 입력해주세요.",
          },
        }}
      />
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <AuthInput
            onBlur={onBlur}
            onChangeText={(value: string) => onChange(value)}
            value={value}
            placeholder="비밀번호"
            secureTextEntry
            returnKeyType="done"
            ref={passwordRef}
            lastOne={true}
            onSubmitEditing={handleSubmit(onSubmit)}
          />
        )}
        name="password"
        rules={{ required: true }}
      />

      <AuthButton
        text="가입하기"
        disabled={!isValid}
        onPress={handleSubmit(onSubmit)}
        loading={loading}
      />
    </AuthLayout>
  );
}
