import React from "react";
import styled from "styled-components/native";
import { logUserOut } from "../apollo";
import AuthButton from "../components/auth/AuthButton";
import ScreenLayout from "../components/ScreenLayout";

const ButtonContainer = styled.View`
  margin-top: 20px;
`;

const SText = styled.Text`
  color: ${(props) => props.theme.fontColor};
`;

export default function Notifications({ navigation }: any) {
  const onSubmit = () => {
    console.log("Log Out");
    logUserOut();
  };
  const seePhoto = () => navigation.navigate("Photo");

  return (
    <ScreenLayout>
      <SText>Notification</SText>
      <ButtonContainer>
        <AuthButton text="seePhoto" onPress={seePhoto} />
      </ButtonContainer>
    </ScreenLayout>
  );
}
