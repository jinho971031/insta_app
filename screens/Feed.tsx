import React from "react";
import { defaultClient } from "../apollo";
import styled, { ThemeContext } from "styled-components/native";

import ScreenLayout from "../components/ScreenLayout";
import { FEED_QUERY } from "../gql/queries";
import {
  ActivityIndicator,
  FlatList,
  Image,
  Text,
  View,
  StatusBar,
} from "react-native";
import Photo from "./Photo";
import Logo from "../components/Logo";

import { MaterialCommunityIcons, Ionicons } from "@expo/vector-icons";
import { useContext } from "react";
import Storys from "./Storys";
import { useQuery } from "@apollo/client";

const Header = styled.View`
  width: 100%;
  height: 45px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 30px;

  padding: 0 10px 7px 10px;
`;

const LogoBox = styled.View``;

const IconBox = styled.View`
  flex-direction: row;
`;

const Icon = styled.TouchableOpacity`
  margin-left: 20px;
`;

const iconSize = 26;

//Storys--------------------------------------

export default function Feed({ navigation }: any) {
  const { fontColor } = useContext(ThemeContext);

  const { data: Feed, loading } = useQuery(FEED_QUERY);

  console.log(Feed);

  const renderPhoto = ({ item: photo }: any) => {
    return <Photo photo={photo} />;
  };

  // return (
  //   <ScreenLayout loading={loading}>
  //     <Header>
  //       <LogoBox>
  //         <Logo width="120px" height="40px" />
  //       </LogoBox>
  //       <IconBox>
  //         <Icon>
  //           <Ionicons
  //             name="add-circle-outline"
  //             color={fontColor}
  //             size={iconSize}
  //           />
  //         </Icon>
  //         <Icon>
  //           <Ionicons name="heart-outline" color={fontColor} size={iconSize} />
  //         </Icon>
  //         <Icon>
  //           <Ionicons
  //             name="paper-plane-outline"
  //             color={fontColor}
  //             size={iconSize}
  //           />
  //         </Icon>
  //       </IconBox>
  //     </Header>
  //     <FlatList
  //       data={Feed}
  //       keyExtractor={(photo) => "" + photo.id}
  //       renderItem={renderPhoto}
  //       showsVerticalScrollIndicator={false}
  //       ListHeaderComponent={Storys}
  //       style={{ width: "100%" }}
  //     />
  //   </ScreenLayout>
  // );
}

/*
{Feed.map((photo: any) => (
        <Photo photo={photo} key={photo.id} />
      ))}
*/
