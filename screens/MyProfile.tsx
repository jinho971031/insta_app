import { useQuery } from "@apollo/client";
import React from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import { defaultClient, logUserOut } from "../apollo";

import AuthButton from "../components/auth/AuthButton";
import ScreenLayout2 from "../components/ScreenLayout2";
import { SharedText } from "../components/Shared";
import { ME_QUERY } from "../gql/queries";

const Main = styled.View`
  margin-top: 10px;
  padding: 15px;
`;

// const Article = styled.View``;

const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const HeaderLeft = styled.View``;

const HeaderRight = styled.View``;

const Username = styled(SharedText)`
  font-size: 26px;
  font-weight: 600;
`;

const ButtonContainer = styled.View`
  margin-top: 20px;
`;

const Contents = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 10px;
`;

const ContentsRight = styled.View`
  width: 200px;
  flex-direction: row;
  justify-content: space-between;
`;

const MainInfo = styled.Text`
  color: ${(props) => props.theme.fontColor};
  margin-left: 15px;
`;

const SImage = styled.Image`
  width: 80px;
  height: 80px;
  border-radius: 50px;
`;

export default function MyProfile() {
  const onSubmit = () => {
    console.log("Log Out");
    logUserOut();
  };

  const { me } = useQuery(ME_QUERY);
  console.log(me);

  return (
    <ScreenLayout2>
      <Main>
        <Header>
          <HeaderLeft>
            <Username>{me.username}</Username>
          </HeaderLeft>
          <HeaderRight>
            <Username>ICON</Username>
          </HeaderRight>
        </Header>
        <Contents>
          <SImage source={{ uri: me.avatar }} />
          <ContentsRight>
            <MainInfo>게시물</MainInfo>
            <MainInfo>팔로워</MainInfo>
            <MainInfo>팔로잉</MainInfo>
          </ContentsRight>
        </Contents>
      </Main>
      <ButtonContainer>
        <AuthButton text="로그아웃" onPress={onSubmit} />
      </ButtonContainer>
    </ScreenLayout2>
  );
}
