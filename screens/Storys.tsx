import React from "react";
import { ScrollView } from "react-native";
import styled from "styled-components/native";

const Container = styled.View`
  padding: 10px;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

const Story = styled.View`
  align-items: center;
  padding: 10px;
`;

const ProfileView = styled.View`
  width: 75px;
  height: 75px;
  border-radius: 40px;
  background-color: ${(props) => props.theme.white};
`;

const Username = styled.Text`
  color: ${(props) => props.theme.fontColor};
  margin-top: 10px;
`;

export default function Storys() {
  return (
    <Container>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <Story>
          <ProfileView />
          <Username>내 스토리</Username>
        </Story>
        <Story>
          <ProfileView />
          <Username>내 스토리</Username>
        </Story>
        <Story>
          <ProfileView />
          <Username>내 스토리</Username>
        </Story>
        <Story>
          <ProfileView />
          <Username>내 스토리</Username>
        </Story>
        <Story>
          <ProfileView />
          <Username>내 스토리</Username>
        </Story>
        <Story>
          <ProfileView />
          <Username>내 스토리</Username>
        </Story>
      </ScrollView>
    </Container>
  );
}
