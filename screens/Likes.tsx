import React from "react";
import { Text } from "react-native";
import styled from "styled-components/native";
import ScreenLayout from "../components/ScreenLayout";
import { SharedText } from "../components/Shared";

const Container = styled.View``;

export default function Likes() {
  return (
    <ScreenLayout>
      <Container>
        <SharedText>Likes</SharedText>
      </Container>
    </ScreenLayout>
  );
}
