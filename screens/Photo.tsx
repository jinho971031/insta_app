import React, { useState, useContext } from "react";
import styled, { ThemeContext } from "styled-components/native";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import Modal from "react-native-modal";

import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  useWindowDimensions,
} from "react-native";
import { gql, useMutation } from "@apollo/client";
import { PHOTO_LIKE_MUTATION } from "../gql/mutations";
import { FEED_QUERY } from "../gql/queries";
import { useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import CreateComment from "../components/CreateComment";

const iconSize = 30;

const Container = styled.View`
  width: 100%;
`;

const Header = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px 7px;
  background-color: ${(props) => props.theme.bgColor};
  border: 0.5px solid ${(props) => props.theme.bgColor};
  border-bottom-color: ${(props) => props.theme.borderColor};
`;

const HeaderLeft = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`;

const CommonText = styled.Text`
  color: ${(props) => props.theme.fontColor};
  font-size: 16px;
`;

const SText = styled.Text`
  color: ${(props) => props.theme.fontColor};
  font-weight: 600;
  margin-right: 5px;
`;

const LinkText = styled.Text`
  color: ${(props) => props.theme.fontColor};
  opacity: 0.5;
`;

const CommentLink = styled(LinkText)`
  margin-bottom: 5px;
`;

const ProfileImageView = styled.View`
  width: 36px;
  height: 36px;
  justify-content: center;
  align-items: center;
  border: 0.5px solid gray;
  border-radius: 20px;
  margin-right: 10px;
`;

const ProfileImage = styled.Image`
  width: 33px;
  height: 33px;
  border-radius: 19px;
`;

// More Modal-----------------------------------------------

const MoreButton = styled.TouchableOpacity``;

const ModalView = styled.View`
  width: 100%;
  background-color: ${(props) => props.theme.modalbgColor};
  position: absolute;
  bottom: 0;
  padding: 10px 15px 15px 15px;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
`;

const ModalClose = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  padding: 5px 0 30px 0;
`;

const ModalCloseIcon = styled.View`
  background-color: ${(props) => props.theme.modalboxColor};
  width: 40px;
  height: 5px;
  border-radius: 3px;
`;

const ModalMenu1 = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
`;

const ModalMenu2 = styled.View`
  border-radius: 10px;
`;

const ModalButton1 = styled.TouchableOpacity`
  width: 31%;
  justify-content: center;
  align-items: center;
  padding: 15px;
  background-color: ${(props) => props.theme.modalboxColor};
  border-radius: 10px;
`;

const ModalButton2 = styled.TouchableOpacity`
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.modalboxColor};
  padding: 16px;
`;

const ModalButtonFirst = styled(ModalButton2)`
  border-style: solid;
  border-width: 0.5px;
  border-color: ${(props) => props.theme.modalboxColor};
  border-bottom-color: ${(props) => props.theme.modalbdColor};
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
`;

const ModalButtonLast = styled(ModalButton2)`
  border-style: solid;
  border-width: 0.5px;
  border-color: ${(props) => props.theme.modalboxColor};
  border-top-color: ${(props) => props.theme.modalbdColor};
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
`;

const ModalText = styled.Text`
  color: ${(props) => props.theme.fontColor};
  text-align: center;
  margin-top: 10px;
`;

const ModalText2 = styled.Text`
  color: ${(props) => props.theme.fontColor};
  font-size: 16px;
  text-align: center;
`;

//---------------------------------------------------------------

//Contents-------------------------------------------------------

const FileImage = styled.Image`
  width: 100%;
  height: 100%;
`;

const Contents = styled.View`
  width: 100%;
  background-color: ${(props) => props.theme.bgColor};
  padding: 10px;
`;

const IconsView = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  padding-bottom: 10px;
`;

const LeftIcons = styled.View`
  flex-direction: row;
  width: 50%;
`;

const Icon = styled.TouchableOpacity`
  margin-right: 10px;
`;

const LikeView = styled.TouchableOpacity`
  margin-bottom: 10px;
`;

const ArticleView = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: 5px;
`;

const Comments = styled.View`
  margin-bottom: 10px;
`;

const Comment = styled.View`
  flex-direction: row;
  align-items: center;
`;

export default function Photo({
  photo: { id, user, file, isLiked, likeCount, caption, comments },
}: PhotoProps) {
  const { width, height } = useWindowDimensions();
  const [imageHeight, setImageHeight] = useState(height - 200);
  useEffect(() => {
    Image.getSize(file, (width, height) => {
      if (height > 800) {
        setImageHeight(height / 3);
      } else if (height > 550) {
        setImageHeight(height / 2);
      } else {
        setImageHeight(height);
      }
    });
  }, [file]);

  const navigation = useNavigation();

  const { fontColor } = useContext(ThemeContext);
  const [modalVisible, setModalVisible] = useState(false);

  const updatePhotoLike = (cache: any, result: any) => {
    const {
      data: {
        togglePhotoLike: { ok },
      },
    } = result;
    console.log(ok);

    const fragmentId = `Photo:${id}`;
    const fragment = gql`
      fragment BSName on Photo {
        isLiked
        likeCount
      }
    `;
    if (ok) {
      const result = cache.readQuery({
        query: FEED_QUERY,
      });

      if ("isLiked" in result && "likeCount" in result) {
        const { isLiked: cacheIsLiked, likeCount: cacheLikeCount } = result;
        cache.writeQuery({
          query: FEED_QUERY,
          data: {
            isLiked: !cacheIsLiked,
            likeCount: cacheIsLiked ? cacheLikeCount - 1 : cacheLikeCount + 1,
          },
        });
      }
    }
  };

  const [photolike] = useMutation(PHOTO_LIKE_MUTATION, {
    update: updatePhotoLike,
    variables: { photoId: id },
  });

  return (
    <Container>
      <Header>
        <HeaderLeft onPress={() => navigation.navigate("Profile", { user })}>
          <ProfileImageView>
            <ProfileImage source={{ uri: user.avatar }} />
          </ProfileImageView>
          <SText>{user.username}</SText>
        </HeaderLeft>
        <MoreButton onPress={() => setModalVisible(true)}>
          <Ionicons name="ellipsis-horizontal" color={fontColor} size={24} />
        </MoreButton>
      </Header>

      <Modal
        isVisible={modalVisible}
        swipeDirection="down"
        onSwipeComplete={(e) => setModalVisible(false)}
        style={{ width: "100%", margin: 0 }}
      >
        <ModalView>
          <ModalClose>
            <ModalCloseIcon />
          </ModalClose>
          <ModalMenu1>
            <ModalButton1>
              <Ionicons
                name="share-outline"
                color={fontColor}
                size={iconSize}
              />

              <ModalText>공유 대상...</ModalText>
            </ModalButton1>
            <ModalButton1>
              <MaterialCommunityIcons
                name="link-variant"
                color={fontColor}
                size={iconSize}
              />

              <ModalText>링크 복사</ModalText>
            </ModalButton1>
            <ModalButton1>
              <MaterialCommunityIcons
                name="alert-octagon-outline"
                color="red"
                size={iconSize}
              />

              <Text style={styles.alertText}>신고</Text>
            </ModalButton1>
          </ModalMenu1>
          <ModalMenu2>
            <ModalButtonFirst>
              <ModalText2>게시물 알림 설정</ModalText2>
            </ModalButtonFirst>
            <ModalButton2>
              <ModalText2>숨기기</ModalText2>
            </ModalButton2>
            <ModalButtonLast>
              <ModalText2>팔로우 취소</ModalText2>
            </ModalButtonLast>
          </ModalMenu2>
        </ModalView>
      </Modal>

      <FileImage
        resizeMode="cover"
        source={{ uri: file }}
        style={{ width, height: imageHeight }}
      />
      <Contents>
        <IconsView>
          <LeftIcons>
            <Icon onPress={() => photolike()}>
              {isLiked ? (
                <Ionicons name="heart" color="tomato" size={iconSize} />
              ) : (
                <Ionicons
                  name="heart-outline"
                  color={fontColor}
                  size={iconSize}
                />
              )}
            </Icon>
            <Icon>
              <Ionicons
                name="chatbubble-outline"
                color={fontColor}
                size={iconSize}
              />
            </Icon>
            <Icon>
              <Ionicons
                name="heart-outline"
                color={fontColor}
                size={iconSize}
              />
            </Icon>
          </LeftIcons>
          <TouchableOpacity>
            <Ionicons
              name="bookmark-outline"
              color={fontColor}
              size={iconSize}
            />
          </TouchableOpacity>
        </IconsView>
        <LikeView onPress={() => navigation.navigate("Likes")}>
          {likeCount > 0 && <SText>좋아요 {likeCount}개</SText>}
        </LikeView>
        <ArticleView>
          <SText>{user.username}</SText>
          <CommonText>{caption}</CommonText>
        </ArticleView>
        <Comments>
          <CommentLink>댓글 개 모두보기</CommentLink>
          <Comment>
            <SText>{user.username}</SText>
            <CommonText>{comments[0].comment}</CommonText>
          </Comment>
        </Comments>
        <CreateComment user={user} />
      </Contents>
    </Container>
  );
}

const styles = StyleSheet.create({
  alertText: {
    color: "tomato",
  },
});
