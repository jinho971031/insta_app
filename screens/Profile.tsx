import React from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import { defaultClient, logUserOut } from "../apollo";

import AuthButton from "../components/auth/AuthButton";
import ScreenLayout2 from "../components/ScreenLayout2";
import { SharedText } from "../components/Shared";
import { ME_QUERY } from "../gql/queries";

const Main = styled.View`
  margin-top: 15px;
  padding: 15px;
`;

// const Article = styled.View``;

const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const HeaderLeft = styled.View`
  width: 40px;
`;
const HeaderMid = styled.View``;

const HeaderRight = styled.View``;

const Username = styled(SharedText)`
  font-size: 20px;
  font-weight: 600;
`;

const ButtonContainer = styled.View`
  margin-top: 20px;
`;

const Contents = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 10px;
`;

const ContentsRight = styled.View`
  width: 200px;
  flex-direction: row;
  justify-content: space-between;
`;

const MainInfo = styled.Text`
  color: ${(props) => props.theme.fontColor};
  margin-left: 15px;
`;

const SImage = styled.Image`
  width: 80px;
  height: 80px;
  border-radius: 50px;
`;

export default function Profile(params: any) {
  const onSubmit = () => {
    console.log("Log Out");
    logUserOut();
  };

  const { user } = params.route.params;
  return (
    <ScreenLayout2>
      <Main>
        {/* ////////////////////// */}
        <Header>
          <HeaderLeft></HeaderLeft>
          <HeaderMid>
            <Username>{user.username}</Username>
          </HeaderMid>
          <HeaderRight>
            <Username>ICON</Username>
          </HeaderRight>
        </Header>
        {/* /////////////////////// */}

        <Contents>
          <SImage source={{ uri: user.avatar }} />
          <ContentsRight>
            <MainInfo>게시물</MainInfo>
            <MainInfo>팔로워</MainInfo>
            <MainInfo>팔로잉</MainInfo>
          </ContentsRight>
        </Contents>
      </Main>
    </ScreenLayout2>
  );
}
