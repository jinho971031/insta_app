import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styled from "styled-components/native";
import { logUserOut } from "../apollo";

import AuthButton from "../components/auth/AuthButton";

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.bgColor};
`;

const ButtonContainer = styled.View`
  margin-top: 20px;
`;

export default function Market() {
  const onSubmit = () => {
    console.log("Log Out");
    logUserOut();
  };
  return (
    <Container>
      <Text>Hello</Text>
      <ButtonContainer>
        <AuthButton text="로그아웃" onPress={onSubmit} />
      </ButtonContainer>
    </Container>
  );
}
