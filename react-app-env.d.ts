interface AuthButtonProps {
  disabled?: boolean;
  text: string;
  onPress: () => void;
  loading?: boolean;
}

interface InputProps {
  lastOne?: boolean;
}

interface LinkProps {
  direc?: string;
}

// useForm

interface LoginFormData {
  username: string;
  password: string;
}

interface SignUpFormData {
  email: string;
  name: string;
  username: string;
  password: string;
}

interface CommentFormData {
  comment: string;
}

//------------------------------
interface PhotoProps {
  photo: {
    id: number;
    user: any;
    file: string;
    isLiked: boolean;
    likeCount: number;
    caption: string;
    comments: any;
  };
}
