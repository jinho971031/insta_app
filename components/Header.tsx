import React from "react";
import styled, { ThemeContext } from "styled-components/native";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";

const StyleView = styled.View`
  width: 100%;
  position: absolute;
  top: 0;
  padding-top: 20px;
`;
const StyleText = styled.Text`
  color: white;
`;

export default function Header() {
  return (
    <StyleView>
      <StyleText>
        <FontAwesomeIcon icon={faArrowLeft} color="white" />
      </StyleText>
      <StyleText>what</StyleText>
    </StyleView>
  );
}
