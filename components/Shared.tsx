import styled from "styled-components/native";

export const SharedText = styled.Text`
  color: ${(props) => props.theme.fontColor};
  font-size: 14px;
`;

export const SharedLink = styled.Text<LinkProps>`
  font-size: 12px;
  font-weight: 600;
  color: ${(props) => props.theme.blue};
`;
