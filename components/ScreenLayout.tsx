import React, { useContext } from "react";
import { ActivityIndicator } from "react-native";
import styled, { ThemeContext } from "styled-components/native";

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.bgColor};
`;

export default function ScreenLayout({ children, loading }: any) {
  const { fontColor } = useContext(ThemeContext);
  return (
    <Container>
      {loading ? <ActivityIndicator color={fontColor} /> : children}
    </Container>
  );
}
