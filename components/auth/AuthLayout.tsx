import React from "react";
import { Platform, TouchableWithoutFeedback, Keyboard } from "react-native";
import styled from "styled-components/native";
import Logo from "../Logo";

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.bgColor};
  padding: 0 20px;
`;

const KeyboardAV = styled.KeyboardAvoidingView`
  width: 100%;
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export default function AuthLayout({ children }: any) {
  const dismissKeyboard = () => {
    Keyboard.dismiss();
  };
  return (
    <TouchableWithoutFeedback
      style={{ flex: 1 }}
      onPress={dismissKeyboard}
      disabled={Platform.OS === "web"}
    >
      <Container>
        <KeyboardAV
          behavior="padding"
          keyboardVerticalOffset={Platform.OS === "ios" ? 40 : 0}
        >
          <Logo />
          {children}
        </KeyboardAV>
      </Container>
    </TouchableWithoutFeedback>
  );
}
