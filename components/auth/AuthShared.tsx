import styled from "styled-components/native";

export const AuthInput = styled.TextInput<InputProps>`
  width: 100%;

  background-color: ${(props) => props.theme.boxColor};
  color: ${(props) => props.theme.fontColor};
  border: 0.5px solid ${(props) => props.theme.borderColor};
  border-radius: 5px;

  margin-bottom: ${(props) => (props.lastOne ? "15px" : "10px")};

  padding: 15px;
`;
