import React from "react";
import styled from "styled-components/native";

const SeperatorView = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 30px 0;
`;

const InnerView = styled.View`
  width: 47%;
  height: 0.5px;
  background-color: ${(props) => props.theme.borderColor};
  overflow: hidden;
`;

const SText = styled.Text`
  color: ${(props) => props.theme.gray};
  font-size: 12px;
  font-weight: 600;
  padding: 0 10px;
`;

export default function Seperator() {
  return (
    <SeperatorView>
      <InnerView />
      <SText>또는</SText>
      <InnerView />
    </SeperatorView>
  );
}
