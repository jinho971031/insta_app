import React from "react";
import styled from "styled-components/native";

interface FormError {
  message?: string;
  ref?: any;
}

const SFormError = styled.Text`
  color: tomato;
  font-weight: 600;
  font-size: 12px;
  margin-bottom: 10px;
`;

export default function FormError({ message, ref }: FormError) {
  return <SFormError>{message}</SFormError>;
}
