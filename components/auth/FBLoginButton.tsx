import React from "react";
import styled from "styled-components/native";
import { faFacebookSquare } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { SharedLink } from "../Shared";

const Container = styled.TouchableOpacity`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 10px;
`;

const SText = styled.Text`
  margin-right: 10px;
`;

const FBLink = styled(SharedLink)`
  font-size: 14px;
`;

export default function FBLoginButton() {
  return (
    <Container onPress={() => {}}>
      <SText>
        <FontAwesomeIcon icon={faFacebookSquare} color="#0095f6" size={20} />
      </SText>
      <FBLink>Facebook으로 로그인</FBLink>
    </Container>
  );
}
