import React, { useState } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Modal,
  TextInput,
} from "react-native";
import styled from "styled-components/native";
import Emoji from "react-native-emoji";
import { Ionicons } from "@expo/vector-icons";
import { AuthInput } from "./auth/AuthShared";
import { Keyboard } from "react-native";
import { Controller, useForm } from "react-hook-form";

const Container = styled.KeyboardAvoidingView`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 5px;
`;

const LinkText = styled.Text`
  color: ${(props) => props.theme.fontColor};
  opacity: 0.5;
`;

const EmojiView = styled.View`
  flex-direction: row;
`;

const OPC = styled.TouchableOpacity`
  padding: 5px;
`;

const CommonText = styled.Text`
  color: ${(props) => props.theme.fontColor};
  font-size: 16px;
`;

export default function CreateComment({ user }: any) {
  const [createCommentVisible, setCreateCommentVisible] = useState(false);
  //   Keyboard.addListener("keyboardWillShow", () => setCreateCommentVisible(true));
  const { control, handleSubmit, formState, getValues } =
    useForm<CommentFormData>({
      mode: "onChange",
    });
  const { errors, isValid } = formState;

  return (
    <Container behavior="padding" keyboardVerticalOffset={40}>
      <View style={{ flexDirection: "row" }}>
        <Image
          source={{ uri: user.avatar }}
          style={{ width: 30, height: 30, borderRadius: 20, marginRight: 10 }}
        />
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              onBlur={onBlur}
              onChangeText={(value: string) => onChange(value)}
              value={value}
              placeholder="댓글 달기..."
              keyboardType="email-address"
              returnKeyType="done"
            />
          )}
          name="comment"
          rules={{
            required: true,
            maxLength: {
              value: 100,
              message: "100자 이내로 작성해주세요.",
            },
            pattern: {
              value: /^[0-9a-zA-Z._%+-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,6}$/i,
              message: "이메일을 형식에 맞게 작성해주세요.",
            },
          }}
        />
        {errors?.comment?.message ? (
          <FormError message={errors.comment.message} />
        ) : null}
        <EmojiView>
          <OPC onPress={() => setCreateCommentVisible(true)}>
            <CommonText>
              <Emoji name="heart" style={{ fontSize: 12 }} />
            </CommonText>
          </OPC>
          <OPC onPress={() => setCreateCommentVisible(true)}>
            <CommonText>
              <Emoji name="raised_hands" style={{ fontSize: 12 }} />
            </CommonText>
          </OPC>
          <OPC onPress={() => setCreateCommentVisible(true)}>
            <Ionicons name="add-circle-outline" color={"#8e8e8e"} size={16} />
          </OPC>
        </EmojiView>
        <Modal
          visible={createCommentVisible}
          animationType="slide"
          transparent={true}
          style={{ width: "100%", margin: 0, alignItems: "flex-end" }}
        >
          <KeyboardAvoidingView
            behavior="position"
            enabled
            style={{
              width: "100%",
              height: 50,
              backgroundColor: "green",
            }}
          >
            <TouchableOpacity
              onPress={() => setCreateCommentVisible(false)}
              style={{ width: 100, height: 100, backgroundColor: "blue" }}
            ></TouchableOpacity>
          </KeyboardAvoidingView>
        </Modal>
      </View>
    </Container>
  );
}

/*
<View style={{ marginRight: 10 }}>
        <Image
          source={{ uri: user.avatar }}
          style={{ width: 30, height: 30, borderRadius: 20 }}
        />
      </View>
      <TouchableOpacity
        onPress={() => setCreateCommentVisible(true)}
        style={{ width: "65%", height: 30, justifyContent: "center" }}
      >
        <LinkText>댓글 달기...</LinkText>
      </TouchableOpacity>
      <EmojiView>
        <OPC onPress={() => setCreateCommentVisible(true)}>
          <CommonText>
            <Emoji name="heart" style={{ fontSize: 12 }} />
          </CommonText>
        </OPC>
        <OPC onPress={() => setCreateCommentVisible(true)}>
          <CommonText>
            <Emoji name="raised_hands" style={{ fontSize: 12 }} />
          </CommonText>
        </OPC>
        <OPC onPress={() => setCreateCommentVisible(true)}>
          <Ionicons name="add-circle-outline" color={"#8e8e8e"} size={16} />
        </OPC>
      </EmojiView>
      <Modal
        visible={createCommentVisible}
        animationType="slide"
        transparent={true}
        style={{ width: "100%", margin: 0 }}
      >
        <KeyboardAvoidingView
          style={{
            width: "100%",
            height: 50,
            backgroundColor: "green",
            position: "absolute",
            bottom: 0,
          }}
        >
          <TouchableOpacity
            onPress={() => setCreateCommentVisible(false)}
            style={{ width: 100, height: 100, backgroundColor: "blue" }}
          ></TouchableOpacity>
        </KeyboardAvoidingView>
      </Modal>



*/
