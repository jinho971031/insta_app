import React, { useContext } from "react";
import { Image, StyleSheet, View } from "react-native";
import styled, { ThemeContext } from "styled-components/native";

const StyleView = styled.View``;

const SLogo = styled.Image`
  width: ${(props) => (props.width ? props.width : "200px")};
  height: ${(props) => (props.height ? props.height : "128px")};
`;

export default function Logo({ width, height }: any) {
  const { dark } = useContext(ThemeContext);

  return (
    <StyleView>
      {dark ? (
        <SLogo
          width={width}
          height={height}
          resizeMode="contain"
          source={require("../assets/logo_white.png")}
        />
      ) : (
        <SLogo
          width={width}
          height={height}
          resizeMode="contain"
          source={require("../assets/logo.png")}
        />
      )}
    </StyleView>
  );
}
