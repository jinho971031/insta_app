import React from "react";
import styled from "styled-components/native";

import { logUserOut } from "../apollo";

import AuthButton from "./auth/AuthButton";

const ButtonContainer = styled.View`
  margin-top: 20px;
`;

export default function LogOut() {
  return (
    <ButtonContainer>
      <AuthButton text="로그아웃" onPress={logUserOut} />
    </ButtonContainer>
  );
}
